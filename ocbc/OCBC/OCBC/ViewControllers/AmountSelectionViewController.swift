//
//  AmountSelectionViewController.swift
//  OCBC
//
//  Created by Hostel Hunting on 22/04/2020.
//  Copyright © 2020 Nagarjuna. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class AmountSelectionViewController: SuperVC {
    
    var amount = ""
    var listOfUsers:ListOfUser? = nil
    var cellSelection = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        LocalData.shared.getAllUser({ (usersArray) in
            self.listOfUsers = usersArray
        })
        
    
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        self.collectionView!.register(TextLabel.self, forCellWithReuseIdentifier: TextLabel.ID)
        self.collectionView!.register(ButtonCell.self, forCellWithReuseIdentifier: ButtonCell.ID)
        

        


        // Do any additional setup after loading the view.
    }
    
    @objc func sendAmount()
    {
        let enteredAmount = Int(amount)
        
        if enteredAmount == nil {
            return
        }
        
        if cellSelection == -1 {
            return
        }
        
        let user = listOfUsers?.users[cellSelection]
        print(user?.id)
        print(user?.amount)

        
//        LocalData.shared.sendAmount(with: enteredAmount ?? 0) { (status, message) in
//            Toast.showToastMessage(title: "Amount", subtitle: message, style: .error)
//            self.collectionView.reloadData()
//        }
    }
    // MARK: UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        switch section {
        case 0:
            return 1
        case 1:
            return self.listOfUsers?.users.count ?? 0
        case 2:
            return 1
        default:
            return 0
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        
        switch indexPath.section {
            
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TextFieldCell.ID, for: indexPath) as! TextFieldCell
            
            cell.textField.placeholder = "Enter Amount"
            cell.textField.tag = indexPath.row
            cell.textField.delegate = self
            return cell
        case 1:
            let textLabel = collectionView.dequeueReusableCell(withReuseIdentifier: TextLabel.ID, for: indexPath) as! TextLabel
            
            if indexPath.row == cellSelection {
                
                
                let messageText = NSMutableAttributedString(string: "\(self.listOfUsers?.users[indexPath.row].name ?? "")\n", attributes: [NSAttributedString.Key.font: UIFont(name: Font.boldItalic, size: 20)!,NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)])
                
                textLabel.label.attributedText = messageText


            }else{
            let messageText = NSMutableAttributedString(string: "\(self.listOfUsers?.users[indexPath.row].name ?? "")\n", attributes: [NSAttributedString.Key.font: UIFont(name: Font.normal, size: 20)!,NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)])
                textLabel.label.attributedText = messageText

            
            }
            
            return textLabel
        case 2:
            let sendBtnCell = collectionView.dequeueReusableCell(withReuseIdentifier: ButtonCell.ID, for: indexPath) as! ButtonCell
            
            sendBtnCell.button.setTitle("Send", for: .normal)
            sendBtnCell.button.titleLabel?.font = Font.ButtonFont
            sendBtnCell.button.backgroundColor = MainColor
            sendBtnCell.button.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
            
            sendBtnCell.button.addTarget(self, action: #selector(sendAmount), for: .touchUpInside)
            
            return sendBtnCell

            
        default:
            break
        }
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        cellSelection = indexPath.row
        self.collectionView.reloadData()
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if indexPath.section == 0
        {
            return CGSize.init(width: screenWidth, height: 50)
        }else
        {
            return CGSize.init(width: screenWidth - 40, height: 60)
        }
        //return CGSize.zero
    }

    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension AmountSelectionViewController:UITextFieldDelegate
{
    // MARK: Textfield Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: String = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        print(currentString)
        switch textField.tag {
        case 0:
            amount = currentString
            
        default:
            break
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.returnKeyType == .next
        {
            textField.becomeFirstResponder()
            
        }else if textField.returnKeyType == .done
        {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
}
