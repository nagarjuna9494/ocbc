//
//  RegistrationVC.swift
//  OCBC
//
//  Created by gs reddy on 20/04/20.
//  Copyright © 2020 Nagarjuna. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class RegistrationVC: SuperVC {
    
    var userName = ""
    var password = ""
    var confirmPassword = ""
    
    var buttonTitle = "Sign Up"
    
    let placeHolderTitle = ["User Name","Password","Confirm password"]
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        switch section {
        case 0:
            return 1
        case 1:
            return 3
        case 2:
          return 2
        default:
            return 0
        }
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
    
        // Configure the cell
        switch indexPath.section {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCell.ID, for: indexPath) as! ImageCell
            cell.backgroundColor = .clear
            return cell
            
        case 1:
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TextFieldCell.ID, for: indexPath) as! TextFieldCell
                
                cell.textField.placeholder = placeHolderTitle[indexPath.row]
                cell.textField.tag = indexPath.row
                cell.textField.delegate = self
                //cell.textField.keyboardType = .phonePad
                
            switch indexPath.row {
            case 0:
                cell.textField.text = userName
                cell.textField.returnKeyType = .next
            case 1:
                cell.textField.text = password
                cell.textField.isSecureTextEntry = true
                cell.textField.returnKeyType = .next
            case 2:
                cell.textField.text = confirmPassword
                cell.textField.isSecureTextEntry = true
                cell.textField.returnKeyType = .done
            default:
                break
            }
            
            //cell.textField.attributedPlaceholder = NSAttributedString(string: placeHolderTitle[indexPath.row], attributes: [NSAttributedString.Key.foregroundColor : LineColor.withAlphaComponent(0.8)])
            
                return  cell
        case 2:
            if indexPath.row == 0
            {
            let singInButtonCell = collectionView.dequeueReusableCell(withReuseIdentifier: ButtonCell.ID, for: indexPath) as! ButtonCell
            
                singInButtonCell.button.setTitle(self.buttonTitle, for: .normal)
            singInButtonCell.button.titleLabel?.font = Font.ButtonFont
            singInButtonCell.button.backgroundColor = MainColor
            singInButtonCell.button.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
                
            singInButtonCell.button.addTarget(self, action: #selector(singUpA), for: .touchUpInside)
                
            return singInButtonCell
            }else
            {
                let contactUsButtonCell = collectionView.dequeueReusableCell(withReuseIdentifier: ButtonCell.ID, for: indexPath) as! ButtonCell
                
                contactUsButtonCell.backView.backgroundColor = .clear
                let attributedText = NSMutableAttributedString()
                attributedText.append(NSAttributedString.init(string: "I have an account? ", attributes: [NSAttributedString.Key.font: UIFont.init(name: Font.normal, size: 14)!,NSAttributedString.Key.foregroundColor:TitleColor.withAlphaComponent(0.8)]))
                
                
                attributedText.append(NSAttributedString(string: "Login", attributes: [NSAttributedString.Key.font: UIFont.init(name: Font.medium, size: 16)!,NSAttributedString.Key.foregroundColor:MainColor]))
                contactUsButtonCell.button.setAttributedTitle(attributedText, for: .normal)
                
                contactUsButtonCell.button.addTarget(self, action: #selector(logInA), for: .touchUpInside)
                
                return contactUsButtonCell
            }
            
        default:
            break
        }
    
        return cell
    }

    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if indexPath.section == 0
        {
            return CGSize.init(width: screenWidth, height: 220)
        }else
        {
            return CGSize.init(width: screenWidth - 40, height: 60)
        }
        //return CGSize.zero
    }
    
    // MARK: Actions
    @objc func logInA()
    {
        self.navigationController?.popViewController(animated: true)

    }
    
    
    
    @objc func singUpA()
    {
        if userName == "" || password == "" || confirmPassword == ""
        {
            Toast.showToastMessage(title: "Sign Up", subtitle: "Please enter the missing fields", style: .error)
            return
        }
        
        if confirmPassword != password
        {
            Toast.showToastMessage(title: "Sign Up", subtitle: "confirm password' and 'password' do not match", style: .error)
            return
        }
        
        
        let user = User()
        user.name = userName.lowercased()
        user.password = password
        
        LocalData.shared.createNewUser(with: user) { (status, message) in
            if status == true
            {
                Toast.showToastMessage(title: "Sign Up", subtitle: message, style: .error)
                
                guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
                  let sceneDelegate = windowScene.delegate as? SceneDelegate
                else {
                  return
                }
                let profileVC = ProfileVC()
                profileVC.title = "Profile"
                let nav = UINavigationController.init(rootViewController: profileVC)
                sceneDelegate.window?.rootViewController = nav
                sceneDelegate.window?.makeKeyAndVisible()
                
            }else
            {
                Toast.showToastMessage(title: "Sign Up", subtitle: message, style: .error)
            }

        }
        
        
        
        
        
        
    }
    
    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
extension RegistrationVC:UITextFieldDelegate
{
    // MARK: Textfield Delegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let currentString: String = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        print(currentString)
        switch textField.tag {
        case 0:
            userName = currentString
        case 1:
            password = currentString
        case 2:
           confirmPassword = currentString
            
        default:
            break
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField.returnKeyType == .next
        {
            textField.becomeFirstResponder()
            
        }else if textField.returnKeyType == .done
        {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
}

extension UIViewController {

    func presentAlertWithTitle(title: String, message: String, options: String..., completion: @escaping (Int) -> Void) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, option) in options.enumerated() {
            alertController.addAction(UIAlertAction.init(title: option, style: .default, handler: { (action) in
                completion(index)
            }))
        }
        self.present(alertController, animated: true, completion: nil)
    }
}




