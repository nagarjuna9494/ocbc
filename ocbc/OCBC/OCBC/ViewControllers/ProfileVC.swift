//
//  ProfileVC.swift
//  OCBC
//
//  Created by gs reddy on 20/04/20.
//  Copyright © 2020 Nagarjuna. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class ProfileVC: SuperVC {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        let allUsers = LocalData.shared.realm.objects(User.self).sorted(byKeyPath: "id")
        print(allUsers)

        // Register cell classes
        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        
        self.collectionView!.register(ProfileCell.self, forCellWithReuseIdentifier: ProfileCell.ID)

        // Do any additional setup after loading the view.
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.navigationBar.isHidden = false
        
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return 4
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
    
        // Configure the cell
        switch indexPath.row {
        case 0:
            
            let profileCell = collectionView.dequeueReusableCell(withReuseIdentifier: ProfileCell.ID, for: indexPath) as! ProfileCell
            
            let messageText = NSMutableAttributedString(string: "\(loginProfile.name.capitalized)\n", attributes: [NSAttributedString.Key.font: UIFont(name: Font.normal, size: 20)!,NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)])
            
            messageText.append(NSAttributedString(string: "\(loginProfile.amount)", attributes: [NSAttributedString.Key.font: UIFont(name: Font.medium, size: 30)!,NSAttributedString.Key.foregroundColor:#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)]))
            
            profileCell.titleL.attributedText = messageText
            profileCell.titleL.textAlignment = .center
        
            return profileCell
            
        case 1:
            let addAmountButtonCell = collectionView.dequeueReusableCell(withReuseIdentifier: ButtonCell.ID, for: indexPath) as! ButtonCell
            
                addAmountButtonCell.button.setTitle("Add Amount", for: .normal)
            addAmountButtonCell.button.titleLabel?.font = Font.ButtonFont
            addAmountButtonCell.button.backgroundColor = MainColor
            addAmountButtonCell.button.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
            addAmountButtonCell.button.addTarget(self, action: #selector(addAmount), for: .touchUpInside)
            addAmountButtonCell.button.tag = 1
                
            return addAmountButtonCell
        case 2:
        let sendAmountButtonCell = collectionView.dequeueReusableCell(withReuseIdentifier: ButtonCell.ID, for: indexPath) as! ButtonCell
        
        sendAmountButtonCell.button.setTitle("Send Amount", for: .normal)
        sendAmountButtonCell.button.titleLabel?.font = Font.ButtonFont
        sendAmountButtonCell.button.backgroundColor = MainColor
        sendAmountButtonCell.button.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
        sendAmountButtonCell.button.addTarget(self, action: #selector(sendAmount), for: .touchUpInside)
        sendAmountButtonCell.button.tag = 2
            
        return sendAmountButtonCell
            
        case 3:
            let logoutButtonCell = collectionView.dequeueReusableCell(withReuseIdentifier: ButtonCell.ID, for: indexPath) as! ButtonCell
            
            logoutButtonCell.button.setTitle("Logout", for: .normal)
            logoutButtonCell.button.titleLabel?.font = Font.ButtonFont
            logoutButtonCell.button.backgroundColor = MainColor
            logoutButtonCell.button.setTitleColor(#colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), for: .normal)
            logoutButtonCell.button.addTarget(self, action: #selector(logoutAction), for: .touchUpInside)
            logoutButtonCell.button.tag = 2
            
            return logoutButtonCell
            
        default:
            break
        }
        
    
        return cell
    }

    // MARK: UICollectionViewDelegate

    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        if indexPath.row == 0
        {
            return CGSize.init(width: screenWidth, height: 220)
        }else if indexPath.row == 1
        {
            return CGSize.init(width: screenWidth - 40, height: 60)
        }else if indexPath.row == 2
        {
            return CGSize.init(width: screenWidth - 40, height: 60)
        }else if indexPath.row == 3
        {
            return CGSize.init(width: screenWidth - 40, height: 60)
        }
        return CGSize.zero
    }
    
    // MARK : Actions
    @objc func addAmount(_ button:UIButton)
    {
        
        let alertController = UIAlertController(title: "Add Amount", message: "", preferredStyle: .alert)

        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter Amount"
            textField.keyboardType = .numberPad

        }

        let addAction = UIAlertAction(title: "Add", style: .default, handler: { alert -> Void in
            let amountField = alertController.textFields![0] as UITextField
            
            let amount = Int(amountField.text ?? "0")
            
            if amount == nil {
                return
            }
        
            if button.tag == 2
            {
                return
            }
            LocalData.shared.addAmount(with: amount ?? 0) { (status, message) in
                Toast.showToastMessage(title: "Amount", subtitle: message, style: .success)
                self.collectionView.reloadData()
            }
        })

        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil )


        alertController.addAction(addAction)
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
        
        
        
       
    }
    
    
    @objc func  logoutAction(_ button:UIButton)
    {
        loginProfile = User()
        
        guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene,
            let sceneDelegate = windowScene.delegate as? SceneDelegate
            else {
                return
        }
        let loginVC = LoginVC()
        let nav = UINavigationController.init(rootViewController: loginVC)
        sceneDelegate.window?.rootViewController = nav
        sceneDelegate.window?.makeKeyAndVisible()
        
    }
    
    @objc func sendAmount(_ button:UIButton)
    {
//        if button.tag == 1
//        {
//            return
//        }
//        LocalData.shared.sendAmount(with: 100) { (status, message) in
//            Toast.showToastMessage(title: "Amount", subtitle: message, style: .error)
//            self.collectionView.reloadData()
//        }
        
        let amountSelectionVC = AmountSelectionViewController()
        self.navigationController?.pushViewController(amountSelectionVC, animated: true)
        
    }
    
    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
