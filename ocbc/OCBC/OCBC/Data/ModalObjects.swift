//
//  ModalObjects.swift
//  OCBC
//
//  Created by Hostel Hunting on 20/04/20.
//  Copyright © 2020 Nagarjuna. All rights reserved.
//

import UIKit
import RealmSwift
import ObjectMapper

class User: Object,Mappable {
    
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var phone = ""
    @objc dynamic var amount = 0
    @objc dynamic var password = ""
    @objc dynamic var selected = false
    @objc dynamic var penddingAmount = 0
    
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        phone <- map["phone"]
        name <- map["name"]
        amount <- map["amount"]
        password <- map["password"]
        selected <- map["selected"]
        penddingAmount <- map["penddingAmount"]
        
    }
}

class ListOfUser: Object {
    
     var users = List<User>()
   
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        users <- map["users"]
       
    }
}

class transactionHistory: Object,Mappable {
    
    @objc dynamic var id = ""
    @objc dynamic var senderId = ""
    @objc dynamic var recivedId = ""
    @objc dynamic var senderName = ""
    @objc dynamic var recivedName = ""
    @objc dynamic var amount = 0
    
    override class func primaryKey() -> String? {
        return "id"
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
        id <- map["id"]
        senderId <- map["senderId"]
        recivedId <- map["recivedId"]
        amount <- map["amount"]
        senderName <- map["senderName"]
        recivedName <- map["recivedName"]
        
    }
}
