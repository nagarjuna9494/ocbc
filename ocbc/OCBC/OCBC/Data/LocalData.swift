//
//  LocalData.swift
//  OCBC
//
//  Created by Hostel Hunting on 20/04/20.
//  Copyright © 2020 Nagarjuna. All rights reserved.
//

import UIKit
import RealmSwift

class LocalData: NSObject {
    static let shared: LocalData = LocalData()
    let realm = try! Realm()
    
    
    @objc func createNewUser(with user: User, completion: @escaping (Bool,String) -> Swift.Void)
    {
//        if self.realm.objects(User.self).count >= 2
//        {
//            completion(false,"Max members accounts are exit.")
//            return
//        }
        
        if let user = self.realm.objects(User.self).filter("name == '\(user.name.lowercased())'").last
        {
            completion(false,"\(user.name) already exit.")
        }else
        {
        let newId = (realm.objects(User.self).max(ofProperty: "id") as Int? ?? 0) + 1
          
            try! self.realm.write
            {
                user.id = newId
                user.selected = false
                self.realm.add(user)
                
                loginProfile = user
                completion(true,"\(user.name) welcome...")
            }
            
        }
    }
    
    @objc func findUser(with user: User, completion: @escaping (Bool,String) -> Swift.Void)
    {
        if let profile = self.realm.objects(User.self).filter("name == '\(user.name.lowercased())'").last
        {
            if profile.password == user.password
            {
                loginProfile = profile
                completion(true,"Successfully login")
            }else
            {
                completion(false,"Password was wrong!")
            }
        }else
        {
            completion(false,"User name don't exit")
        }
    }
    
    @objc func getAllUser(_ completion:@escaping (ListOfUser) -> Swift.Void)
       {
        let allUsers = self.realm.objects(User.self).filter("name != '\(loginProfile.name.lowercased())'").sorted(byKeyPath: "id")
        print(allUsers)
        let liUsers = ListOfUser()
        liUsers.users.append(objectsIn: allUsers)
        completion(liUsers)
        
        

//        completion(userArray ?? [])
       
       }
    
    @objc func addAmount(with amount: Int, completion: @escaping (Bool,String) -> Swift.Void)
    {
        if loginProfile.name == ""
        {
            completion(false,"not login")
        }
        
        if let notLoginprofile = self.realm.objects(User.self).filter("name != '\(loginProfile.name.lowercased())'").last
        {
            try! self.realm.write
            {
                if loginProfile.penddingAmount >= 0
                {
                    if loginProfile.penddingAmount - amount >= 0
                    {
                        loginProfile.penddingAmount = loginProfile.penddingAmount - amount
                        //notLoginprofile.amount = notLoginprofile.amount + loginProfile.penddingAmount
                        loginProfile.amount = loginProfile.amount + amount
                        notLoginprofile.amount = notLoginprofile.amount + amount
                        
                    }else if loginProfile.penddingAmount - amount <= 0
                    {
                        
                        let diff = amount - loginProfile.penddingAmount
                        loginProfile.amount = loginProfile.amount + diff
                        loginProfile.penddingAmount = 0
                    }
                }else
                {
                    loginProfile.amount = loginProfile.amount + amount
                }
                
                self.realm.add(notLoginprofile, update: .modified)
                completion(true,"Succefully added amount")
            }
        }else if let loginpro = self.realm.objects(User.self).filter("name == '\(loginProfile.name.lowercased())'").last
            {
                try! self.realm.write
                {
                  loginpro.amount = loginpro.amount + amount
                  self.realm.add(loginpro, update: .modified)
                  loginProfile = loginpro
                  completion(true,"Succefully added amount")
                }
        }else
        {
            completion(false,"Please try again")
        }
    }
    
    
    @objc func sendAmountToUser(with amount:Int,user:User, completion:@escaping (Bool) -> Swift.Void) {
        
        if let profile = self.realm.objects(User.self).filter("id != '\(user.id)'").last
        {
            try! self.realm.write
            {
                
            }
                
        }
        
    }
    @objc func sendAmount(with amount: Int, completion: @escaping (Bool,String) -> Swift.Void)
    {
        if loginProfile.name == ""
        {
            completion(false,"not login.")
            return
        }
        
        if let profile = self.realm.objects(User.self).filter("name != '\(loginProfile.name.lowercased())'").last
        {
            try! self.realm.write
            {
                if loginProfile.amount < amount
                {
                    loginProfile.penddingAmount = amount - loginProfile.amount
                    loginProfile.amount = loginProfile.amount - amount
                    
                }else
                {
                  profile.amount = profile.amount + amount
                  loginProfile.amount = loginProfile.amount - amount
                }
                self.realm.add(profile, update: .modified)
                //self.realm.add(loginProfile, update: .modified)
                completion(true,"message")
            }
        }else
        {
            completion(false,"Please another user not there.")
        }
    }
    
}
