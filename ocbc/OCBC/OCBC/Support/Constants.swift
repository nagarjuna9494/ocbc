//
//  Constants.swift
//  OCBC
//
//  Created by Hostel Hunting on 20/04/20.
//  Copyright © 2020 Nagarjuna. All rights reserved.
//

import UIKit

let screenBounds = UIScreen.main.bounds
let screenSize   = screenBounds.size
let screenWidth  = screenSize.width
let screenHeight = screenSize.height
let gridWidth : CGFloat = ((screenSize.width - 40 - 15)/2)
let navigationHeight : CGFloat = 44.0
let statubarHeight : CGFloat = 20.0
let navigationHeaderAndStatusbarHeight : CGFloat = navigationHeight + statubarHeight

var MainColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)//"34B7F1"
var SecondaryColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
var AppLinkColor = "0096FF"
var HintColor = #colorLiteral(red: 0.6, green: 0.6, blue: 0.6, alpha: 1)//"A6AEB7"
var LineColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)//"D9D8DF"

let TitleColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)//"5C6774"
let SubTitleColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)//41464D

var loginProfile = User()

class Font {
    
    static let HeaderFontSize = 18
    static let buttonFontSize = 16
    static let textFieldFontSize = 14
    static let BodyFontSize = 14
    static let BodyInfoFontSize = 12
    
    //, 24pt, 22pt, 20pt, 18pt, 16pt, 14pt
    
    //Helvetica Neue
    static let ultraLightItalic = "HelveticaNeue-UltraLightItalic"
    static let medium = "HelveticaNeue-Medium"
    static let mediumItalic = "HelveticaNeue-MediumItalic"
    static let ultraLight = "HelveticaNeue-UltraLight"
    static let italic = "HelveticaNeue-Italic"
    static let light = "HelveticaNeue-Light"
    static let thinItalic = "HelveticaNeue-ThinItalic"
    static let lightItalic = "HelveticaNeue-LightItalic"
    static let bold = "Lato-Bold"
    static let thin = "HelveticaNeue-Thin"
    static let condensedBlack = "HelveticaNeue-CondensedBlack"
    static let normal = "HelveticaNeue"
    static let condensedBold = "HelveticaNeue-CondensedBold"
    static let boldItalic = "HelveticaNeue-BoldItalic"
    
    static let appFont = "HelveticaNeue"
    
    
    static let ButtonFont = UIFont.init(name: Font.medium, size: CGFloat(Font.BodyFontSize))!
    
    static let HeaderFont = UIFont.init(name: Font.medium, size: CGFloat(Font.HeaderFontSize))!
    
    static let titleFont = UIFont.init(name: Font.normal, size: CGFloat(Font.BodyInfoFontSize))!
    
    static let subTitleFont = UIFont.init(name: Font.normal, size: CGFloat(12))!
   
    
}
