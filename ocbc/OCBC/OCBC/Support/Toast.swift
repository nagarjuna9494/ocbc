//
//  Toast.swift
//  OCBC
//
//  Created by gs reddy on 20/04/20.
//  Copyright © 2020 Nagarjuna. All rights reserved.
//

import UIKit
import Foundation
import SwiftMessages

class Toast: NSObject {
    class func showToastMessage(title: String, subtitle: String,style:Theme)
    {
        let view = MessageView.viewFromNib(layout: .cardView)
        var config = SwiftMessages.defaultConfig
        
        view.configureContent(title: title, body: subtitle, iconImage: nil, iconText: nil, buttonImage: nil, buttonTitle: "Ok", buttonTapHandler: { _ in SwiftMessages.hide() })
        view.configureTheme(style, iconStyle: .default)
        view.configureDropShadow()
        config.duration = .seconds(seconds: 2)
        view.button?.isHidden = true
        view.button?.titleLabel?.text = ""
        view.iconImageView?.isHidden = true
        view.iconLabel?.isHidden = true
        config.presentationStyle = .top
        config.interactiveHide = true
        SwiftMessages.show(config: config, view: view)
        
    }
}
