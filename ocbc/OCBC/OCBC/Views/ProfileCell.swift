//
//  ProfileCell.swift
//  OCBC
//
//  Created by gs reddy on 20/04/20.
//  Copyright © 2020 Nagarjuna. All rights reserved.
//

import UIKit

class ProfileCell: DatasourceCell
{
    static let ID = "ProfileCellID"
    
    let backView : UIView = {
        let bv = UIView()
        bv.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).withAlphaComponent(0.1)
        bv.layer.cornerRadius = 50
        bv.layer.masksToBounds = true
        return bv
    }()
    
    let titleL : UILabel = {
        let title = UILabel()
        title.backgroundColor = .clear
        title.numberOfLines = 0
        return title
    }()
    
    
    let imageV: UIImageView = {
        let imageV = UIImageView()
        imageV.image = #imageLiteral(resourceName: "download-1")
        imageV.contentMode = .scaleAspectFit
        imageV.layer.cornerRadius = 50
        imageV.layer.masksToBounds = true
        imageV.shadowStyle()
        
        return imageV
    }()
    
    
    override func setupViews() {
        super.setupViews()
        
        separatorLineView.isHidden = true
        separatorLineView.backgroundColor = UIColor(r: 230, g: 230, b: 230)
        
        addSubview(backView)
        backView.addSubview(imageV)
        backView.addSubview(titleL)
        
        backView.anchor(self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 5, leftConstant: 10, bottomConstant: 5, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        
        imageV.anchor(self.topAnchor, left: self.leftAnchor, bottom: nil, right: nil, topConstant: 10, leftConstant: (screenWidth / 2) - 50, bottomConstant: 0, rightConstant: 0, widthConstant: 100, heightConstant: 100)
        
        titleL.anchor(imageV.bottomAnchor, left: backView.leftAnchor, bottom: backView.bottomAnchor, right: backView.rightAnchor, topConstant: 5, leftConstant: 10, bottomConstant: 5, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        
    }
    
    
    
}
