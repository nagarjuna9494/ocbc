//
//  ButtonCell.swift
//  OCBC
//
//  Created by Hostel Hunting on 20/04/20.
//  Copyright © 2020 Nagarjuna. All rights reserved.
//

import UIKit

class ButtonCell: DatasourceCell
{
    static let ID = "ButtonCellId"
    
    let backView : UIView = {
        let bv = UIView()
        bv.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0).withAlphaComponent(0.1)
        bv.layer.cornerRadius = 10
        bv.layer.masksToBounds = true
        return bv
    }()
    
    let titleL : UILabel = {
        let title = UILabel()
        
        return title
    }()
    
    let button: UIButton = {
        let buttonC = UIButton()
        buttonC.titleLabel?.font = UIFont.init(name: Font.normal, size: 14)
        buttonC.layer.cornerRadius = 10
        buttonC.layer.masksToBounds = true
        
        return buttonC
    }()
    
    override func setupViews() {
        super.setupViews()
        
        addSubview(backView)
        backView.addSubview(button)
        
        backView.anchor(self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 5, leftConstant: 10, bottomConstant: 5, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        button.anchor(backView.topAnchor, left: backView.leftAnchor, bottom: backView.bottomAnchor, right: backView.rightAnchor, topConstant: 0, leftConstant: 0, bottomConstant: 0, rightConstant: 0, widthConstant: 0, heightConstant: 0)
        
    }
    
    
    
}
