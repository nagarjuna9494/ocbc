//
//  TextFieldCell.swift
//  OCBC
//
//  Created by Hostel Hunting on 20/04/20.
//  Copyright © 2020 Nagarjuna. All rights reserved.
//

import UIKit

class TextFieldCell: DatasourceCell
{
    static let ID = "TextFieldCellId"

    let textField : UITextField = {
        let tF = UITextField()
        tF.placeholder = "Enter Agenda "
        tF.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        tF.font = UIFont.init(name: Font.normal, size: 16)
        tF.attributedPlaceholder = NSAttributedString(string: "Enter Agenda", attributes: [NSAttributedString.Key.foregroundColor : UIColor(white: 0.6, alpha: 0.5)])
        tF.layer.cornerRadius = 5
        
        return tF
    }()
    
    let backView : UIView = {
        let bv = UIView()
        bv.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1).withAlphaComponent(0.1)
        bv.layer.cornerRadius = 10
        bv.layer.masksToBounds = true
        return bv
    }()
    
    
    override func setupViews() {
        super.setupViews()
        
        addSubview(backView)
        backView.addSubview(textField)
        
        backView.anchor(self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 5, leftConstant: 10, bottomConstant: 5, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        
        textField.anchor(backView.topAnchor, left: backView.leftAnchor, bottom: backView.bottomAnchor, right: backView.rightAnchor, topConstant: 5, leftConstant: 10, bottomConstant: 5, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        
    }
    
    
    
}
