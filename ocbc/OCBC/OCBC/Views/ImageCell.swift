//
//  ImageCell.swift
//  OCBC
//
//  Created by gs reddy on 20/04/20.
//  Copyright © 2020 Nagarjuna. All rights reserved.
//

import UIKit

class ImageCell: DatasourceCell
{
    static let ID = "ImageCellId"
    
    let imageV: UIImageView = {
        let imageV = UIImageView()
        imageV.image = #imageLiteral(resourceName: "download-1")
        imageV.contentMode = .scaleAspectFit
        //imageV.layer.cornerRadius = 50
        //imageV.layer.masksToBounds = true
        
        return imageV
    }()
    
    override func setupViews()
    {
        super.setupViews()
        
        separatorLineView.isHidden = true
        separatorLineView.backgroundColor = UIColor(r: 230, g: 230, b: 230)
        backgroundColor = .white
        
        addSubview(imageV)
        
        imageV.anchor(self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 5, leftConstant: 10, bottomConstant: 5, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        
        
    }
    
    
}


class TextLabel: DatasourceCell {
    
    static let ID = "TextLabelID"
    
    let label: UILabel = {
        let title = UILabel()
        title.backgroundColor = .clear
        title.numberOfLines = 0
        return title
        
    }()
    
    
    override func setupViews()
    {
        super.setupViews()
        
        separatorLineView.isHidden = true
        separatorLineView.backgroundColor = UIColor(r: 230, g: 230, b: 230)
        backgroundColor = .white
        
        addSubview(label)
        
        label.anchor(self.topAnchor, left: self.leftAnchor, bottom: self.bottomAnchor, right: self.rightAnchor, topConstant: 5, leftConstant: 10, bottomConstant: 5, rightConstant: 10, widthConstant: 0, heightConstant: 0)
        
        
    }
    
}
